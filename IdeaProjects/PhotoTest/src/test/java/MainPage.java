import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import java.io.File;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class MainPage {
    private final SelenideElement imgInput = $x("//input[@type=\"file\"]");
    public MainPage(String url) {
        Selenide.open(url);
        sleep(1000);

    }
    public MainPage uploadImage(File img){
        imgInput.uploadFile(img);
        sleep(5000);
        $x("//button[@class=\"button files__button button_dark\"]").click();
        sleep(5000);
        return this;
    }
}
