import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;


public class MyTest extends BaseTest {
    private final static String MY_URL = "https://jpg2pdf.com/ru/";
    private final static File MY_IMG = new File("imagebigbrain.jpg");
    //Хромдрайвер качает файлы в папку build\downloads оборачивая их ещё в одну папку
    //Поэтому нужен метод для подсчета этих папок
    public static int countFoldersInDirectory(String directoryPath) {
        File directory = new File(directoryPath);

        if (directory.exists() && directory.isDirectory()) {
            File[] subdirectories = directory.listFiles(File::isDirectory);
            return subdirectories != null ? subdirectories.length : 0;
        } else {
            System.out.println("Указанная директория не существует или не является директорией");
            return 0;
        }
    }
    @Test
    public void testUpload() {
        int inCounter = countFoldersInDirectory("build/downloads");
        System.out.println(inCounter+ " до скачивания");
        MainPage mainPage = new MainPage(MY_URL);
        System.out.println("Зашли на сайт");
        mainPage.uploadImage(MY_IMG);
        System.out.println("Загрузили наш файл(картинку)");
        int outCounter = countFoldersInDirectory("build/downloads");
        System.out.println(outCounter+ " после скачивания");
        System.out.println("Проверим, что количество файлов в папке загрузки увеличилось на 1");
        Assertions.assertEquals(inCounter+1, outCounter);

    }


}